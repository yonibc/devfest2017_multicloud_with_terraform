variable "azurerm_location" {
  default     = "Southeast Asia"
}

variable "aws_setup_wekan_path" {
  default     = "/tmp/setup_wekan.sh"
}
