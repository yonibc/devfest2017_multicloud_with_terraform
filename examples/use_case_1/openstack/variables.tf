variable "image" {
  default = "Ubuntu 16.04"
}

variable "flavor" {
  default = "m1.small"
}

variable "ssh_key_file" {
  default = "~/.ssh/id_rsa.terraform"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "external_gateway" {}

variable "pool" {
  default = "public"
}

variable "setup_wekan_path" {
  default = "/tmp/setup_wekan.sh"
}
