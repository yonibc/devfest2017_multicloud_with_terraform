provider "azurerm" {
  subscription_id = "CHANGEME"
  client_id       = "CHANGEME"
  client_secret   = "CHANGEME"
  tenant_id       = "CHANGEME"
}

resource "azurerm_resource_group" "rg" {
  name     = "wekanapp"
  location = "${var.azurerm_location}"
}

resource "azurerm_virtual_network" "net" {
  name                = "wekanapp-net"
  address_space       = ["10.0.0.0/16"]
  location            = "${var.azurerm_location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
}

resource "azurerm_subnet" "subnet" {
  name                 = "wekanapp-subnet"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  virtual_network_name = "${azurerm_virtual_network.net.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "iface" {
  name                = "wekan-iface"
  location            = "${var.azurerm_location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"

  ip_configuration {
    name                          = "ipconf"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "dynamic"
  }
}

resource "azurerm_virtual_machine" "app" {
  name                  = "wekan app"
  location              = "${var.azurerm_location}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.test.id}"]
  vm_size               = "Standard_A0"

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "wekanosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "file" {
    source      = "../scripts/setup_wekan.sh"
    destination = "${var.aws_setup_wekan_path}"
  }

  provisioner "local-exec" {
    command = "${var.aws_setup_wekan_path}"
  }
}
